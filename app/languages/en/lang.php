<?php

/**
 * Language: English (EN-US)
 */
$lang = array();

$lang['site_title'] = "iHospital";
$lang['site_meta'] = "iHospital - Hospital Management System";

$lang['firstname'] = "First Name";
$lang['lastname'] = "Last Name";
$lang['email_address'] = "Email Address";
$lang['actions'] = "Actions";
$lang['edit_action'] = "Edit";
$lang['delete_action'] = "Delete";
$lang['no_users_found'] = "No users found.";