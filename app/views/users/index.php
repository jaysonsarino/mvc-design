<html>
<head>
    <title><?=$page_title?></title>
</head>
<body>

    <table border="1">
        <thead>
            <tr>
                <th><?=$this->lang->line('firstname')?></th>
                <th><?=$this->lang->line('lastname')?></th>
                <th><?=$this->lang->line('email_address')?></th>
                <th><?=$this->lang->line('actions')?></th>
            </tr>
        </thead>
        <tbody>
            <?php if( $users ) { foreach( $users as $user ) {?>
            <tr>
                <td><?=$user['firstname']?></td>
                <td><?=$user['lastname']?></td>
                <td><?=$user['email_address']?></td>
                <td>
                    <a href=""><?=$this->lang->line('edit_action')?></a> | 
                    <a href=""><?=$this->lang->line('delete_action')?></a>
                </td>
            </tr>
            <?php } } else { ?>
            <tr>
                <td colspan="5"><?=$this->lang->line('no_users_found')?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</body>
</html>