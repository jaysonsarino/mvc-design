<?php
/**
 * CodePHP Framework
 * 
 *  An open source application development framework for PHP
 * 
 * @package CodePHP
 * @author Jayson Sarino
 * @since Version 1.0.0
 */
namespace iHospital\App\Controllers;

defined("ALLOW_INCLUDES") OR exit("Direct access to this file is not allowed");

/**
 * Config Class
 * 
 * Provides database and 3rd-party applications credentials
 * 
 * @package CodePHP
 * @category Controller
 * @author Jayson Sarino
 */
class Config {

    /**
     * Database hosting or server name
     * 
     * @var String
     */
    const DB_HOST = 'localhost';

    /**
     * Database username
     * 
     * @var String
     */
    const DB_USERNAME = 'root';

    /**
     * Database password
     * 
     * @var String
     */
    const DB_PASSWORD = '';

    /**
     * Database name
     * 
     * @var String
     */
    const DB_NAME = 'ihospital';

}