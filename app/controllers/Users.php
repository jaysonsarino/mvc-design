<?php

namespace iHospital\App\Controllers;

if (!defined("ALLOW_INCLUDES")) {
    die("Direct access to this file is not allowed");
}
use iHospital\App\Core\Controller as controller;
use iHospital\App\Models\User_model as User_model;

class Users extends controller {

    private $data;

    protected $controller;

    public function __construct() {
        parent::__construct();
        $this->controller = new controller();
    }

    public function get($id) {
        $user_model = new User_model();
        $this->data['userDetails'] = $user_model->getDetails($id);
        $this->controller->render("users/view", $this->data);
        
    }

    public function insert($id) {
        $user_model = new User_model();
        $this->data['userDetails'] = $user_model->getDetails($id);
    }

    public function getAll() {
        $user_model = new User_model();
        $this->data['users'] = $user_model->getAll();
        $this->data['page_title'] = $this->lang->line('site_title');
        $this->controller->render("users/index", $this->data);
    }

}