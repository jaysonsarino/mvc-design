<?php
/**
 * CodePHP Framework
 * 
 *  An open source application development framework for PHP
 * 
 * @package CodePHP
 * @author Jayson Sarino
 * @since Version 1.0.0
 */
namespace iHospital\App\Controllers;

defined("ALLOW_INCLUDES") OR exit("Direct access to this file is not allowed");

use iHospital\App\Controllers\Config as Config;

/**
 * Database Class
 * 
 * Connection string to the database 
 * 
 * @package CodePHP
 * @category Controller
 * @author Jayson Sarino
 */
final class Db extends \mysqli{

    /**
     * Instance class
     * 
     * @var String
     */
    private static $instance;

    const TBL_USERS = " `users` ";
    const TBL_DEPARTMENTS = " `departments` ";
    const TBL_PATIENTS = " `patients` ";

    public function __construct() {
        parent::__construct(Config::DB_HOST, Config::DB_USERNAME, Config::DB_PASSWORD, Config::DB_NAME);
        $this->set_charset("utf8mb4");

        if ($this->connect_error) {
            error_log("Connection error: " . $this->connect_error);
            die("Failed to establish database connection");
        }
    }

    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }


}