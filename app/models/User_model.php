<?php

namespace iHospital\App\Models;

use iHospital\App\Core\Model as model;
use iHospital\App\Controllers\Db as db;

if (!defined("ALLOW_INCLUDES")) {
    die("Direct access to this file is not allowed");
}

class User_model extends model{

    protected $tbl_users = Db::TBL_USERS;

    protected $model;

    public function __construct() {
        parent::__construct();
        $this->model = new Model();
    }

    public function getDetails($id) {
        $result = $this->model->singleQuery("SELECT `firstname`, `lastname`, `email_address` FROM " . $this->tbl_users . " WHERE `id`=?", $id);
        return $result;
    }

    public function getAll() {
        $result = $this->model->query("SELECT `firstname`, `lastname`, `email_address` FROM " . $this->tbl_users);
        return $result;
    }

    public function insert_user() {
        $data = [
            'firstname' => 'John',
            'lastname'  =>  'Doe',
            'email_address' => 'johndoe@gmail.com',
            'password' => 'johndoe@123'
        ];
        $result = $this->model->insert($this->tbl_users, $data);
        echo "<pre>".print_r($result, TRUE)."</pre>";
    }

}