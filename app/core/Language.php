<?php 
/**
 * CodePHP Framework
 * 
 *  An open source application development framework for PHP
 * 
 * @package CodePHP
 * @author Jayson Sarino
 * @since Version 1.0.0
 */
namespace iHospital\App\Core;

defined("ALLOW_INCLUDES") OR exit("Direct access to this file is not allowed");

/**
 * Language Class
 * 
 * Core language class
 * 
 * @package CodePHP
 * @category Language
 * @author Jayson Sarino
 */
class Language {

    /**
     * Holds language values
     * 
     * @var String
     */
    public $lang;

    /**
     * Language instance variable
     * 
     * @var String
     */
    public static $instance;

    /**
     * Constructor - core language preferences
     * 
     * @return void
     */
    function __construct() {
        $this->lang = $GLOBALS['lang'];
    }

    /**
     * Get the language value by array key
     * 
     * @return string
     */
    public function line($word) {
        return $this->lang[$word];
    }

    /**
     * Language class get instances
     * 
     * @return void
     */
    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

}