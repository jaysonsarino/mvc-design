<?php 

namespace iHospital\App\Core;

if (!defined("ALLOW_INCLUDES")) {
    die("Direct access to this file is not allowed");
}

use iHospital\App\Controllers\Db as db;

class Model {

    protected $_db; 

    protected $_select;

    public function __construct() {
        $this->_db = Db::getInstance();
    }

    public function select($fields) {
        if( ! is_array($fields) ) {
            $fields = explode(", ", $fields);
        }

        if( count($fields) > 0 ) {
            $field = [];
            foreach( $fields as $f ) {
                array_push( $field, $f );
            }
            $this->select = implode("`, `", $field);
            $this->select = "`" . $this->select . "`";
        }

        return $this->_select;
    }

    public function create( $table, $data = [] ) {
        if( count($data) < 1 ) {
            return false;
        }

        $first = true;
        $vals = array();
        $fields = "";
        $q = "";
        foreach( $data as $k => $v) {

            if($first)
                $fields .="`$k`";
            else
                $fields .=",`$k`";

            if($first)
                $q .="?";
            else
                $q .=",?";

            $vals[] = $v;
            $first = false;

        }

        $stmt = $this->_db->prepare("INSERT INTO " . $table . " (" . $fields . ") VALUES($q)");

        $stmt->execute( $vals );

        $stmt->close();

        return $vals;
    }

    public function singleQuery($sql, $id) {
        $stmt = $this->_db->prepare($sql);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows === 0) {
            return false;
        }
        $stmt->bind_result($firstname, $lastname, $email_address);
        $stmt->fetch();
        $users = array("firstname" => $firstname, "lastname" => $lastname, 'email_address' => $email_address);
        $stmt->free_result();
        $stmt->close();
        return $users;
    }

    public function query($sql) {
        $stmt = $this->_db->prepare($sql);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows === 0) {
            return false;
        }
        $stmt->bind_result($firstname, $lastname, $email_address);
        $users = [];
        while( $stmt->fetch() ) {
            $users[] = array("firstname" => $firstname, "lastname" => $lastname, 'email_address' => $email_address);
        }
        $stmt->free_result();
        $stmt->close();
        return $users;
    }

}