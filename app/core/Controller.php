<?php 
/**
 * CodePHP Framework
 * 
 *  An open source application development framework for PHP
 * 
 * @package CodePHP
 * @author Jayson Sarino
 * @since Version 1.0.0
 */
namespace iHospital\App\Core;

defined("ALLOW_INCLUDES") OR exit("Direct access to this file is not allowed");

use iHospital\App\Core\Model as model;
use iHospital\App\Core\Language as language;
use iHospital\App\Controllers\Db as db;

/**
 * Controller Class
 * 
 * Core controller class
 * 
 * @package CodePHP
 * @category Controller
 * @author Jayson Sarino
 */
class Controller {

    /**
     * Holds language values
     * 
     * @var String
     */
    public $lang;

    /**
     * Instance database variable 
     * 
     * @var String
     */
    protected $db;

    /**
     * Constructor - core controller preferences
     * 
     * @return void
     */
    public function __construct() {
        
        $this->lang = language::getInstance();
        $this->db = Db::getInstance();
    }   

    /**
     * Initialize the view page to render
     * 
     * @param string $template
     * @param array $params
     * @return render
     */
    public function render( $template, $params = [] ) {

        if( file_exists( VIEWPATH . $template . ".php" ) ) {
            if( count($params) > 0 ) {
                foreach($params as $key => $value) {
                    $$key = $value;
                }
            }
            require VIEWPATH . $template . '.php';
        } else {
            require VIEWPATH . '404.php';
            return false;
        }

    }

}