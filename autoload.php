<?php
namespace iHospital;

function autoloader($class) {
    $parts = explode("\\", $class);

    // Skip to other autoloaders immediately
    if ($parts[0] !== __NAMESPACE__) {
        return;
    }

    $controller_file = __DIR__ . DIRECTORY_SEPARATOR . "app/core";
    if ($lastNsPos = strripos($class, '\\')) {
        $namespace = substr($class, 0, $lastNsPos);
        $class = substr($class, $lastNsPos + 1);
        $subNamespace = str_replace(__NAMESPACE__, "", $namespace);
        $controller_file .= str_replace('\\', DIRECTORY_SEPARATOR, $subNamespace) . DIRECTORY_SEPARATOR;
    }

    $controller_file = $controller_file . $class . '.php';

    if (is_file($controller_file)) {
        require_once $controller_file;
    }
}

spl_autoload_register(__NAMESPACE__ . '\autoloader');
