<?php

namespace iHospital;

use iHospital\App\Core\Language as language;

class iHospital { 

    private $dispatcher = null;

    public function __construct() {
        $this->dispatchRoutes();
    }

    private function dispatchRoutes() {
        $this->dispatcher = \FastRoute\simpleDispatcher(function(\FastRoute\RouteCollector $r) {
            $r->addRoute('GET', '/users', function() {
                $users = new App\Controllers\Users();
                return $users->getAll();
            });
            $r->addRoute('GET', '/user/{id:\d+}', function($id) {
                $users = new App\Controllers\Users();
                return $users->get($id);
            });
        });

        $this->init();
    }

    public function init() {
        $httpMethod = $_SERVER['REQUEST_METHOD'];
        $stripped_uri = $uri = $_SERVER['REQUEST_URI'];
        $uri_arr = explode('/', $uri);

        // Strip query string (?foo=bar) and decode URI
        if (false !== $pos = strpos($uri, '?')) {
            $stripped_uri = substr($uri, 0, $pos);
        }
        $stripped_uri = rawurldecode($stripped_uri);

        $routeInfo = $this->dispatcher->dispatch($httpMethod, $stripped_uri);
        switch ($routeInfo[0]) {
            case \FastRoute\Dispatcher::NOT_FOUND:
                echo "Not found";
            case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                $allowedMethods = $routeInfo[1];
                header("HTTP/1.1 405 Method Not Allowed");
                exit;
            case \FastRoute\Dispatcher::FOUND:
                $handler = $routeInfo[1];
                $vars = $routeInfo[2];

                call_user_func_array($handler, $vars);
                exit;
        }

        
    }

}