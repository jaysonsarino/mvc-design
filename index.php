<?php

namespace iHospital;

use iHospital\App\Core\Language as Language;

date_default_timezone_set('UTC');

/**
 * iHospital application entry-point
 */
define("ALLOW_INCLUDES", false);
define("ABSPATH", dirname(__FILE__) . '/');
define("SITE_URL", "http://localhost:1000/");
define("PUBLICPATH", "public/");
define("VIEWPATH", "app/views/");

require_once ABSPATH . 'vendor/autoload.php';
require_once ABSPATH . 'app/libraries/FastRoute/bootstrap.php';
require_once ABSPATH . 'app/languages/en/lang.php';
require_once ABSPATH . 'iHospital.php';

new iHospital();



	

